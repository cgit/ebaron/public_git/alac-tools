#!/bin/sh

# Copyright (C) 2009 Elliott Baron

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

function usage {
	echo "usage: $0 [-c cover_art] flac_file..."
	exit 0
}

if [ "$1" == "-c" ]; then
	if [ $# -ge 2 ]; then
		cover=$2
		shift 2
	else
		usage
	fi
fi
if [ $# -eq 0 ]; then
	usage
fi

basedir=`dirname $0`

for i in "$@"
do
	# Convert from FLAC to ALAC
	flac=$i
	alac=`basename "$flac" .flac`.m4a
	ffmpeg -y -i "$flac" -acodec alac "$alac"
	rc=$?
	if [ $rc -ne 0 ]; then
		exit $rc
	fi
	# Migrate tags
	$basedir/alac_copy_tags.py "$flac" "$alac"
	rc=$?
	if [ $rc -ne 0 ]; then
		exit $rc
	fi
	# Embed Album Art
	if [ ! -z "$cover" ]; then
		$basedir/alac_embed_art.py "$cover" "$alac"
		rc=$?
		if [ $rc -ne 0 ]; then
			exit $rc
		fi
	fi
done
