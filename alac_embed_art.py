#!/usr/bin/env python

# Copyright (C) 2009 Elliott Baron

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
from mutagen.mp4 import MP4

def usage():
	print "usage:", sys.argv[0], "image mp4_file..."

if len(sys.argv) < 2:
	usage()

art = file(sys.argv[1]).read()

for filename in sys.argv[2:]:
	mp4 = MP4(filename)

	# Album Art
	mp4['covr'] = [art]

	mp4.save()
