#!/usr/bin/env python

# Copyright (C) 2009 Elliott Baron

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
from mutagen.mp4 import MP4
from mutagen.flac import FLAC

flac = FLAC(sys.argv[1])
mp4 = MP4(sys.argv[2])

# Track number
trk_num = int(flac['tracknumber'][0])
trk_total = int(flac['tracktotal'][0])
mp4['trkn'] = [(trk_num, trk_total)]
print "Track Number:", mp4['trkn']

# Artist
if 'artist' in flac:
	mp4['\xa9ART'] = flac['artist']
	print "Artist:", mp4['\xa9ART']

# Title
if 'title' in flac:
	mp4['\xa9nam'] = flac['title']
	print "Title:", mp4['\xa9nam']

# Album
if 'album' in flac:
	mp4['\xa9alb'] = flac['album']
	print "Album:", mp4['\xa9alb']

# Year
if 'date' in flac:
	mp4['\xa9day'] = [flac['date'][0][:4]]
	print "Year:", mp4['\xa9day']

# Genre
if 'genre' in flac:
	mp4['\xa9gen'] = flac['genre']
	print "Genre:", mp4['\xa9gen']

mp4.save()
